import HomeLogo from './assets/home.svg?react';
import LibraryLogo from './assets/library.svg?react';
import CommunityLogo from './assets/community.svg?react';
import CloudLogo from './assets/cloud.svg?react';
import StoreLogo from './assets/store.svg?react';
import './App.css';
import './components/NavButton.css';
import Carousel from './components/Carousel';
import NavButton from './components/NavButton';
import GamesList from './components/GamesList';



function App() {

      // fetch("https://displaycatalog.mp.microsoft.com/v7.0/products?bigIds=BQ1W1T1FC14W,C3KLDKZBHNCZ,BS6WJ2L56B10,BRKX5CRMRTC2,9P5S26314HWQ,BX3JNK07Z6QK,BT9FFLG51VVG,C2M8HBNVPT1T,C5K89TFLSV19,BSZM480TSWGP,C299QVC2BSJF,BQMVWCMB8P59&market=US&languages=en-us&MS-CV=DGU1mcuYo0WMMp+F.1")
      //   .then((response) => response.json())
      //   .then((games) => {
      //       // console.log("Game list : ", games)

      //       for (const eachGame of games.Products) {

      //           // let titleGame = eachGame.LocalizedProperties[0].ProductTitle;
      //           // let pictureGame = eachGame.LocalizedProperties[0].Images[0].UnscaledImageSHA256Hash;

      //           console.log("game", eachGame)
      //           // console.log(eachGame.LocalizedProperties[0].ShortTitle, eachGame.LocalizedProperties[0].Images[0].Uri);

      //       }

      //   })
  




  return (
    <div className="wrapper">
      <section className="content">
        <div className="search-bar">
          <input type="text" placeholder="Search.." />
        </div>
        <div className="carousel-container">
          <Carousel />
        </div>

        <div>
          <GamesList/>
        </div>

      </section>
      <aside>
        <nav className="sidebar">
          <NavButton name="Game Pass">
            <HomeLogo className="logo" />
          </NavButton>
          <NavButton name="My library">
            <LibraryLogo className="logo" />
          </NavButton>
          <NavButton name="Cloud Gaming">
            <CommunityLogo className="logo" />
          </NavButton>
          <NavButton name="Community">
            <CloudLogo className="logo" />
          </NavButton>
          <NavButton name="Store">
            <StoreLogo className="logo" />
          </NavButton>
        </nav>
      </aside>
    </div>
  );
}

export default App;

