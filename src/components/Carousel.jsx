import { useState } from 'react';
import CarouselItem from './CarouselItem.jsx';
import './carousel.css';


function Carousel() {

    const [activeIndex, setActiveIndex] = useState(0);

    const gamesFeatured = [

        {
            img: "https://store-images.s-microsoft.com/image/apps.33953.14506879174941978.138d3eab-0b06-443b-a252-c99592521394.fbecd5ff-76a3-4dc9-9005-3799f09950a0?q=90&w=336&h=200"
        },
        {
            img: "https://store-images.s-microsoft.com/image/apps.41797.14316786733461812.bb5a977a-c90a-4411-a25d-25a3b96c55d8.fb3faf50-46b4-4f2d-849f-12b0b4c9b1c7?q=90&w=336&h=200"
        },
        {
            img: "https://store-images.s-microsoft.com/image/apps.31431.13630274674230323.ef522ebd-e0ea-449e-b0c8-3271887caa67.cf5b9e53-458f-4826-aba6-48672bd074d2?q=90&w=336&h=200"
        },
        {
            img: "https://store-images.s-microsoft.com/image/apps.13547.13979868870348147.8ecbeec8-deeb-46af-8803-a1338a261dbd.28f4d315-7d94-4303-be7c-940d3acffd64?q=90&w=336&h=200"
        },

    ]


    const updateIndex = (newIndex) => {
        if (newIndex < 0) {
            newIndex = 0;
        } else if (newIndex >= gamesFeatured.length) {
            newIndex = gamesFeatured.length - 1;
        }

        setActiveIndex(newIndex);
    };


    return (

        <div className="carousel">

            <div className="carousel__featured-img"  style={{ transform: `translate(-${activeIndex * 100}%)`
     }} >

                {gamesFeatured.map((game) => {
                    return <CarouselItem feature={game.img} />
                })}

            </div>
            <div className="carousel-buttons">
                <button
                    className="button-arrow"
                    onClick={() => {
                        updateIndex(activeIndex - 1);
                    }}
                >
                    <span className="material-symbols-outlined">arrow_back_ios</span>{" "}
                </button>
                <div className="indicators">

                    {gamesFeatured.map((game, index) => {
                        return <button className="indicator-buttons"
                            onClick={() => {
                                updateIndex(index);
                            }}>
                            <span
                                className={`material-symbols-outlined ${index === activeIndex
                                    ? "indicator-symbol-active"
                                    : "indicator-symbol"
                                    }`}
                            >
                                radio_button_checked
                            </span>

                        </button>
                    })}

                </div>
                <button
                    className="button-arrow"
                    onClick={() => {
                        updateIndex(activeIndex + 1);
                    }}
                >
                    <span className="material-symbols-outlined">arrow_forward_ios</span>
                </button>
            </div>
        </div>

    );

}

export default Carousel;