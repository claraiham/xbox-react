import React, { useState, useEffect } from 'react';
import './GameList.css'

function GamesList() {
  const [gameList, setGameList] = useState([]);

  useEffect(() => {
    fetch("https://displaycatalog.mp.microsoft.com/v7.0/products?bigIds=BQ1W1T1FC14W,C3KLDKZBHNCZ,BS6WJ2L56B10,BRKX5CRMRTC2,9P5S26314HWQ,BX3JNK07Z6QK,BT9FFLG51VVG,C2M8HBNVPT1T,C5K89TFLSV19,BSZM480TSWGP,C299QVC2BSJF,BQMVWCMB8P59&market=US&languages=en-us&MS-CV=DGU1mcuYo0WMMp+F.1")
      .then((response) => response.json())
      .then((games) => {
        setGameList(games.Products);
      });
  }, []); 

  return (
    <div className="gamesContainer">
      {gameList.map((eachGame) => (
        <div key={eachGame.ProductId} className="displayGames">
          <img src={eachGame.LocalizedProperties[0].Images[1].Uri} alt={eachGame.LocalizedProperties[0].ProductTitle} />
          <p>{eachGame.LocalizedProperties[0].ProductTitle}</p>
        </div>
      ))}
    </div>
  );
}

export default GamesList;
