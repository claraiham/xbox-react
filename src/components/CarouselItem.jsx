function CarouselItem(props) {

    return (

        <div className="carousel-item">
            <div className="carousel">
                <img src={props.feature} />
            </div>
            <div>
                <h3>
                    {props.name}
                </h3>
            </div>

        </div>

    );

}

export default CarouselItem;