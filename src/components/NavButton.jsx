import './NavButton.css';

function NavButton(props) {

    return (

        <button className="button__navbar">
            {props.children}
            {props.name}
        </button>

    );
}

export default NavButton;